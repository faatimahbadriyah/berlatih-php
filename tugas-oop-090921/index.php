<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP PHP</title>
</head>

<body>
    <h1>Berlatih PHP OOP</h1>
    <?php

require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");
echo "Name: " . $sheep->name;
echo '<br>';
echo "legs: " . $sheep->legs;
echo '<br>';
echo "cold blooded: " . $sheep->cold_blooded;
echo '<br><br>';

$frog = new Frog("buduk");
echo "Name: " . $frog->name;
echo '<br>';
echo "legs: " . $frog->legs;
echo '<br>';
echo "cold blooded: " . $frog->cold_blooded;
echo '<br>';
echo "Jump: " . $frog->jump();
echo '<br><br>';

$ape = new Ape("kera sakti");
echo "Name: " . $ape->name;
echo '<br>';
echo "legs: " . $ape->legs;
echo '<br>';
echo "cold blooded: " . $ape->cold_blooded;
echo '<br>';
echo "Yell: " . $ape->yell();
echo '<br><br>';

?>
</body>

</html>